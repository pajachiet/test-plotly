# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# # Téléchargement et préparation des données

import pandas as pd
from pandas.tseries.offsets import DateOffset

df_hospit = pd.read_csv("https://raw.githubusercontent.com/rozierguillaume/covid-19/master/data/france/donnes-hospitalieres-covid19-nouveaux.csv", 
                        sep=";")
df_hospit.jour = pd.to_datetime(df_hospit.jour)
df_hospit = df_hospit.groupby('jour')[["incid_hosp", "incid_rea"]].sum()
df_hospit.head(3)

# +
df_sidep = pd.read_csv("https://raw.githubusercontent.com/rozierguillaume/covid-19/master/data/france/taux-incidence-dep-quot.csv",
                       sep=";", low_memory=False)

df_sidep = df_sidep[df_sidep["cl_age90"] == 0]
df_sidep = df_sidep.rename(columns={"P": "incid_sidep"})
df_sidep.jour = pd.to_datetime(df_sidep.jour)

df_sidep = df_sidep.groupby('jour')[["incid_sidep"]].sum()

df_sidep.head(3)


# -

# # Calcul du R effectif avec méthode simplifiée

def compute_r(df, offset=0):

    df_mean7 = df.groupby('jour').sum().rolling(7, center=True).mean().dropna()
    
    df_mean7_offset7 = df_mean7.copy()
    df_mean7_offset7.index = df_mean7_offset7.index + DateOffset(days=7)
    
    df_R = df_mean7 / df_mean7_offset7
    df_R = pd.DataFrame(df_R).dropna()
    df_R.columns = "R_" + df_R.columns
    
    df_R.index = df_R.index + DateOffset(days=offset)
    
    return df_R


df_hospit_R = compute_r(df_hospit, -15)
df_hospit_R.head(3)

df_sidep_R = compute_r(df_sidep, -5)
df_sidep_R.head(3)

df_R = pd.concat([df_hospit_R, df_sidep_R], axis=1)
df_R.head(3)

# # Visualisation avec plotly

import plotly.express as px
import plotly.graph_objects as go

import plotly.io as pio
pio.renderers.default = "notebook"

# ## Incidences

# ### Incidence journalière tests COVID dans SIDEP

fig = px.bar(df_sidep, y="incid_sidep")
fig.show()

# ### Incidence journalière hospitalisation pour COVID

fig = px.bar(df_hospit, y="incid_hosp")
fig.show()

# ### Incidence journalière réanimation pour COVID

fig = px.bar(df_hospit, y="incid_rea")
fig.show()

# ### R effectif calculé à partir des différentes incidences

# +
fig = px.line(df_R)

fig.show()

# -

# Amélioration de la figure

# +
x_min = df_R.index.min()
x_max = df_R.index.max()

fig.add_shape(type="line",x0=x_min, x1=x_max, y0=1, y1=1,
    line=dict(color="black", width=1))

fig.add_shape(type="line",x0=x_min, x1=x_max, y0=0.7, y1=0.7,
    line=dict(color="grey", width=1, dash="dashdot"))


fig.update_layout(yaxis=dict(range=[0, 2]))

fig.show()

# -



