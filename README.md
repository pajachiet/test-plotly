# Test plotly

Test de plotly avec Jupyter sur un calcul du R effectif à partir de différentes sources de données.  

Pour le voir en ligne sans télécharger le html, vous pouvez utiliser nbviewer à cette adresse : 
https://nbviewer.jupyter.org/urls/gitlab.com/pajachiet/test-plotly/-/raw/master/Reff.ipynb
